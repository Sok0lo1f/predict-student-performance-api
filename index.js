const tf = require('@tensorflow/tfjs')
const sk = require('scikitjs')
const express = require('express')
const bodyParser = require('body-parser')
const { arrayToMark, prepareData, intToMark } = require('./prepareData.js')
const TreeJSONModel = require('./static/dt.json')

sk.setBackend(tf)

const app = express()
const port = 3000
app.use(express.static(`${__dirname}/static`))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

let TreeModel = null

const uploadModel = async () => {
    TreeModel = await sk.fromObject(TreeJSONModel)
    app.listen(port, () => {
        console.log(`Example app listening on port ${port}`)
    })
}

app.post('/predict', async (req, res) => {
    const marks = ['нет оценки', 'не явился', 'зачтено', 'не зачтено', 'не изучал', '2', '3', '4', '5']
    const types = ['зачет', 'экзамен', 'кр', 'практика', 'кп']

    if (!req.body) return res.status(400).json({ detail: 'Body does not exist' })
    const { sex, age, mark, type_input, type_output, code_input, code_output } = req.body
    if (sex !== 0 && sex !== 1) return res.status(400).json({ detail: 'Sex must be 1 (male) or 0 (female)' })
    if (!age || age >= 50 || age <= 16) return res.status(400).json({ detail: 'Age must be between 17 and 49' })
    if (!marks.includes(mark)) return res.status(400).json({ detail: `Mark must be one of: [${marks.join(', ')}]` })
    if (!types.includes(type_input)) return res.status(400).json({ detail: `type_input must be one of: [${types.join(', ')}]` })
    if (!types.includes(type_output)) return res.status(400).json({ detail: `type_output must be one of: [${types.join(', ')}]` })
    if (!code_input) return res.status(400).json({ detail: 'code_input does not exist' })
    if (!code_output) return res.status(400).json({ detail: 'code_output does not exist' })

    const { type } = req.query

    if (type && type !== 'NN' && type !== 'tree') return res.status(400).json({ detail: 'Query TYPE must be NN or tree (default NN)' })

    if (type === 'tree') {
        const preparedData = prepareData(
            {
                sex,
                age,
                mark,
                type_input,
                type_output,
                code_input,
                code_output,
            },
            true
        )

        const prediction = await TreeModel.predict([preparedData])
        try {
            const resp = intToMark(prediction[0])
            return res.json({
                mark: resp,
            })
        } catch (e) {
            return res.status(404).json({ detail: e.message })
        }
    }

    const model = await tf.loadLayersModel('http://localhost:3000/model.json')
    const preparedData = prepareData({
        sex,
        age,
        mark,
        type_input,
        type_output,
        code_input,
        code_output,
    })
    const pred = model.predict(tf.tensor([preparedData])).arraySync()
    try {
        const resp = arrayToMark(pred[0])
        res.json({
            mark: resp,
        })
    } catch (e) {
        res.status(404).json({ detail: e.message })
    }
})

app.post('/predictBySemester', async (req, res) => {
    const marks = ['нет оценки', 'не явился', 'зачтено', 'не зачтено', 'не изучал', '2', '3', '4', '5']
    const types = ['зачет', 'экзамен', 'кр', 'практика', 'кп']

    if (!req.body) return res.status(400).json({ detail: 'Body does not exist' })
    const { semesterInfo } = req.body

    const { age, sex, userMarks, type_output, code_output } = semesterInfo
    if (sex !== 0 && sex !== 1) return res.status(400).json({ detail: 'Sex must be 1 (male) or 0 (female)' })
    if (!age || age >= 50 || age <= 16) return res.status(400).json({ detail: 'Age must be between 17 and 49' })
    if (!types.includes(type_output)) return res.status(400).json({ detail: `type_output must be one of: [${types.join(', ')}]` })
    if (!code_output) return res.status(400).json({ detail: 'code_output does not exist' })
    userMarks.forEach((info) => {
        const { mark, type_input, code_input } = info

        if (!marks.includes(mark)) return res.status(400).json({ detail: `Mark must be one of: [${marks.join(', ')}]` })
        if (!types.includes(type_input)) return res.status(400).json({ detail: `type_input must be one of: [${types.join(', ')}]` })
        if (!code_input) return res.status(400).json({ detail: 'code_input does not exist' })
    })

    const { type } = req.query

    if (type && type !== 'NN' && type !== 'tree') return res.status(400).json({ detail: 'Query TYPE must be NN or tree (default NN)' })

    const answers = {
        'нет оценки': 0,
        'не явился': 0,
        зачтено: 0,
        'не зачтено': 0,
        2: 0,
        3: 0,
        4: 0,
        5: 0,
        'не изучал': 0,
    }
    const response = []
    if (type === 'tree') {
        for (let i = 0; i < userMarks.length; i++) {
            const { mark, type_input, code_input } = userMarks[i]
            const preparedData = prepareData(
                {
                    sex,
                    age,
                    mark,
                    type_input,
                    type_output,
                    code_input,
                    code_output,
                },
                true
            )

            const prediction = await TreeModel.predict([preparedData])
            try {
                const resp = intToMark(prediction[0])
                answers[resp]++
            } catch (e) {
                return res.status(404).json({ detail: e.message })
            }
        }

        Object.keys(answers).forEach((answer) => {
            response.push({
                mark: answer,
                probability: Math.round((answers[answer] / userMarks.length) * 100) / 100,
            })
        })
        return res.json({
            mark: response,
        })
    }

    const model = await tf.loadLayersModel('http://localhost:3000/model.json')

    for (let i = 0; i < userMarks.length; i++) {
        const { mark, type_input, code_input } = userMarks[i]
        const preparedData = prepareData({
            sex,
            age,
            mark,
            type_input,
            type_output,
            code_input,
            code_output,
        })

        const pred = await model.predict(tf.tensor([preparedData])).arraySync()
        try {
            const resp = arrayToMark(pred[0])
            answers[resp]++
        } catch (e) {
            res.status(404).json({ detail: e.message })
        }
    }

    Object.keys(answers).forEach((answer) => {
        response.push({
            mark: answer,
            probability: Math.round((answers[answer] / userMarks.length) * 100) / 100,
        })
    })

    return res.json({
        mark: response,
    })
})

uploadModel()
