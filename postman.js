const _body1 = {
    sex: 1,
    age: 17,
    type_output: 'экзамен',
    code_output: 714211,
    userMarks: [
        {
            mark: 'зачтено',
            type_input: 'зачет',
            code_input: 688318,
        },
        {
            mark: '5',
            type_input: 'кр',
            code_input: 688313,
        },
        {
            mark: 'зачтено',
            type_input: 'зачет',
            code_input: 688316,
        },
        {
            mark: 'зачтено',
            type_input: 'зачет',
            code_input: 688312,
        },
        {
            mark: 'зачтено',
            type_input: 'зачет',
            code_input: 688314,
        },
        {
            mark: '4',
            type_input: 'экзамен',
            code_input: 688321,
        },
        {
            mark: 'зачтено',
            type_input: 'зачет',
            code_input: 688315,
        },
        {
            mark: '5',
            type_input: 'экзамен',
            code_input: 688317,
        },
        {
            mark: 'зачтено',
            type_input: 'зачет',
            code_input: 688320,
        },
        {
            mark: '5',
            type_input: 'экзамен',
            code_input: 688319,
        },
    ],
}
// RIGHT ANSWER = 5
const _body2 = {
    sex: 1,
    age: 18,
    type_output: 'зачет',
    code_output: 687697,
    userMarks: [
        {
            mark: 'зачтено',
            type_input: 'зачет',
            code_input: 642235,
        },
        {
            mark: '4',
            type_input: 'экзамен',
            code_input: 642238,
        },
        {
            mark: '4',
            type_input: 'экзамен',
            code_input: 642229,
        },
        {
            mark: '4',
            type_input: 'экзамен',
            code_input: 642231,
        },
        {
            mark: '4',
            type_input: 'кп',
            code_input: 642232,
        },
        {
            mark: 'зачтено',
            type_input: 'зачет',
            code_input: 642230,
        },
        {
            mark: 'зачтено',
            type_input: 'зачет',
            code_input: 624228,
        },
        {
            mark: '4',
            type_input: 'экзамен',
            code_input: 642236,
        },
        {
            mark: '5',
            type_input: 'практика',
            code_input: 642237,
        },
        {
            mark: 'зачтено',
            type_input: 'зачет',
            code_input: 642233,
        },
        {
            mark: 'зачтено',
            type_input: 'зачет',
            code_input: 642234,
        },
    ],
}
// RIGHT ANSWER = зачтено
const _body3 = {
    sex: 1,
    age: 18,
    type_output: 'зачет',
    code_output: 607162,
    userMarks: [
        {
            mark: 'зачтено',
            type_input: 'зачет',
            code_input: 577712,
        },
        {
            mark: '3',
            type_input: 'экзамен',
            code_input: 577713,
        },
        {
            mark: 'зачтено',
            type_input: 'зачет',
            code_input: 577716,
        },
        {
            mark: 'не явился',
            type_input: 'зачет',
            code_input: 577718,
        },
        {
            mark: '3',
            type_input: 'экзамен',
            code_input: 577715,
        },
        {
            mark: '3',
            type_input: 'экзамен',
            code_input: 577711,
        },
        {
            mark: 'зачтено',
            type_input: 'зачет',
            code_input: 577717,
        },
        {
            mark: '3',
            type_input: 'экзамен',
            code_input: 577709,
        },
        {
            mark: 'зачтено',
            type_input: 'зачет',
            code_input: 577719,
        },
        {
            mark: 'зачтено',
            type_input: 'зачет',
            code_input: 577710,
        },
        {
            mark: '4',
            type_input: 'практика',
            code_input: 577714,
        },
    ],
}
// RIGHT ANSWER = не зачтено

// fetch('http://localhost:3000/predict?type=tree', {
//     method: 'POST',
//     headers: {
//         'Content-Type': 'application/json;charset=utf-8',
//     },
//     body: JSON.stringify({
//         sex: 0,
//         age: 20,
//         mark: '4',
//         type_input: 'экзамен',
//         type_output: 'зачет',
//         code_input: 609349,
//         code_output: 642645,
//     }),
// })
//     .then((res) => res.json())
//     .then((res) => console.log(res))

fetch('http://localhost:3000/predictBySemester?type=tree', {
    method: 'POST',
    headers: {
        'Content-Type': 'application/json;charset=utf-8',
    },
    body: JSON.stringify({
        semesterInfo: _body1,
    }),
})
    .then((res) => res.json())
    .then((res) => console.log(res))
