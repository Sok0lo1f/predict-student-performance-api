const _types = {
    зачет: [0, 0, 0, 0, 1],
    экзамен: [0, 0, 0, 1, 0],
    кр: [0, 0, 1, 0, 0],
    практика: [0, 1, 0, 0, 0],
    кп: [1, 0, 0, 0, 0],
}
const _marks = {
    'нет оценки': [0, 0, 0, 0, 0, 0, 0, 0, 1],
    'не явился': [0, 0, 0, 0, 0, 0, 0, 1, 0],
    зачтено: [0, 0, 0, 0, 0, 0, 1, 0, 0],
    'не зачтено': [0, 0, 0, 0, 0, 1, 0, 0, 0],
    2: [0, 0, 0, 0, 1, 0, 0, 0, 0],
    3: [0, 0, 0, 1, 0, 0, 0, 0, 0],
    4: [0, 0, 1, 0, 0, 0, 0, 0, 0],
    5: [0, 1, 0, 0, 0, 0, 0, 0, 0],
    'не изучал': [1, 0, 0, 0, 0, 0, 0, 0, 0],
}
const _marks_dt = {
    'нет оценки': 0,
    'не явился': 1,
    зачтено: 2,
    'не зачтено': 3,
    2: 4,
    3: 5,
    4: 6,
    5: 7,
    'не изучал': 8,
}
const prepareData = ({ sex, age, code_input, code_output, mark, type_input, type_output }, isDT = false) => {
    if (isDT) return [sex, age / 100, code_input / 1_000_000, ..._types[type_input], _marks_dt[mark], ..._types[type_output], code_output / 1_000_000]
    return [sex, age / 100, code_input / 1_000_000, ..._types[type_input], ..._marks[mark], ..._types[type_output], code_output / 1_000_000]
}
const intToMark = (item) => {
    return Object.keys(_marks_dt).find((key) => _marks_dt[key] === item)
}
const arrayToMark = (arr) => {
    const index = getMinElementIndex(arr)

    const newArr = arr.map((e, i) => {
        if (i === index) return 1
        return 0
    })
    const res = Object.keys(_marks).reduce((prev, key) => {
        if (arrayEquals(_marks[key], newArr)) return (prev = key)
        return prev
    }, 'Error')
    if (res === 'Error') throw new Error('Error: Mark not found')
    return res
}

const getMinElementIndex = (arr) => {
    let index = 0
    let elem = Number.NEGATIVE_INFINITY

    arr.forEach((e, i) => {
        if (e > elem) {
            index = i
            elem = e
        }
    })

    return index
}
const arrayEquals = (a, b) => {
    return Array.isArray(a) && Array.isArray(b) && a.length === b.length && a.every((val, index) => val === b[index])
}

module.exports = {
    prepareData,
    intToMark,
    arrayToMark,
}
