import { describe, it } from 'node:test'
import assert from 'assert'
import { arrayToMark, intToMark, prepareData } from './prepareData.js'

describe('Test prepare data function', () => {
    const mockData = {
        sex: 0,
        age: 20,
        code_input: 123456,
        code_output: 652322,
        mark: 'зачтено',
        type_input: 'зачет',
        type_output: 'экзамен',
    }
    it('should be equal: prepare data with NN type', () => {
        const prepared = prepareData(mockData, false)
        const rightData = [0, 0.2, 0.123456, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0.652322]
        assert.deepEqual(prepared, rightData)
    })
    it('should be equal: prepare data with Tree type', () => {
        const prepared = prepareData(mockData, true)
        const rightData = [0, 0.2, 0.123456, 0, 0, 0, 0, 1, 2, 0, 0, 0, 1, 0, 0.652322]
        assert.deepEqual(prepared, rightData)
    })
    it('should not be equal: prepare data with Tree type', () => {
        const prepared = prepareData(mockData, true)
        const rightData = [0, 0.2, 0.123456, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0.652322]
        assert.notDeepEqual(prepared, rightData)
    })
})

describe('test decoding functions', () => {
    it('should be equal: decode NN prediction', () => {
        const decoded = arrayToMark([0, 0, 1, 0, 0, 0, 0, 0, 0])
        const rightMark = '4'
        assert.deepEqual(decoded, rightMark)
    })
    it('should be equal: decode Tree prediction', () => {
        const decoded = intToMark(6)
        const rightMark = '4'
        assert.deepEqual(decoded, rightMark)
    })
})
