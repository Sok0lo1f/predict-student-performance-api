import app from './index.js'
import { describe, it } from 'node:test'
import assert from 'assert'
import request from 'supertest'

describe('Test Predict Endpoint', () => {
    it('should be error: Sex must be 1 (male) or 0 (female)', async () => {
        const badBody = {
            sex: 2,
        }
        const res = await request(app).post('/predict').send(badBody)
        assert.deepEqual(res.status, 400)
        assert.deepEqual(res.body.detail, 'Sex must be 1 (male) or 0 (female)')
    })
    it('should be error: Age must be between 17 and 49', async () => {
        const badBody = {
            sex: 0,
            age: 1242,
            mark: '123',
            type_input: 'экзаeмен',
            type_output: 'заggчет',
        }
        const res = await request(app).post('/predict').send(badBody)
        assert.deepEqual(res.status, 400)
        assert.deepEqual(res.body.detail, 'Age must be between 17 and 49')
    })
    it('should be error: Mark must be one of', async () => {
        const badBody = {
            sex: 0,
            age: 20,
            mark: '123',
        }
        const res = await request(app).post('/predict').send(badBody)
        assert.deepEqual(res.status, 400)
        assert.deepEqual(res.body.detail, 'Mark must be one of: [нет оценки, не явился, зачтено, не зачтено, не изучал, 2, 3, 4, 5]')
    })
    it('should be error: type_input must be one of', async () => {
        const badBody = {
            sex: 0,
            age: 20,
            mark: '4',
            type_input: 'экзаeмен',
            type_output: 'заggчет',
        }
        const res = await request(app).post('/predict').send(badBody)
        assert.deepEqual(res.status, 400)
        assert.deepEqual(res.body.detail, 'type_input must be one of: [зачет, экзамен, кр, практика, кп]')
    })
    it('should be error: type_output must be one of', async () => {
        const badBody = {
            sex: 0,
            age: 20,
            mark: '4',
            type_input: 'экзамен',
            type_output: 'заggчет',
        }
        const res = await request(app).post('/predict').send(badBody)
        assert.deepEqual(res.status, 400)
        assert.deepEqual(res.body.detail, 'type_output must be one of: [зачет, экзамен, кр, практика, кп]')
    })
    it('should be error: code_input does not exist', async () => {
        const badBody = {
            sex: 0,
            age: 20,
            mark: '4',
            type_input: 'экзамен',
            type_output: 'зачет',
        }
        const res = await request(app).post('/predict').send(badBody)
        assert.deepEqual(res.status, 400)
        assert.deepEqual(res.body.detail, 'code_input does not exist')
    })
    it('should be error: code_output does not exist', async () => {
        const badBody = {
            sex: 0,
            age: 20,
            mark: '4',
            type_input: 'экзамен',
            type_output: 'зачет',
            code_input: 123321,
        }
        const res = await request(app).post('/predict').send(badBody)
        assert.deepEqual(res.status, 400)
        assert.deepEqual(res.body.detail, 'code_output does not exist')
    })
    it('should be error: Query TYPE must be NN or tree (default NN)', async () => {
        const body = {
            sex: 0,
            age: 20,
            mark: '4',
            type_input: 'экзамен',
            type_output: 'зачет',
            code_input: 123321,
            code_output: 123321,
        }
        const res = await request(app).post('/predict?type=test').send(body)
        assert.deepEqual(res.status, 400)
        assert.deepEqual(res.body.detail, 'Query TYPE must be NN or tree (default NN)')
    })
})

describe('Test PredictBySemester Endpoint', () => {
    it('should be error: Sex must be 1 (male) or 0 (female)', async () => {
        const badBody = {
            sex: 10,
            age: 18,
            type_output: 'зачет',
            code_output: 607162,
            userMarks: [
                {
                    mark: 'зачтено',
                    type_input: 'зачет',
                    code_input: 577712,
                },
                {
                    mark: '3',
                    type_input: 'экзамен',
                    code_input: 577713,
                },
                {
                    mark: 'зачтено',
                    type_input: 'зачет',
                    code_input: 577716,
                },
                {
                    mark: 'не явился',
                    type_input: 'зачет',
                    code_input: 577718,
                },
                {
                    mark: '3',
                    type_input: 'экзамен',
                    code_input: 577715,
                },
                {
                    mark: '3',
                    type_input: 'экзамен',
                    code_input: 577711,
                },
                {
                    mark: 'зачтено',
                    type_input: 'зачет',
                    code_input: 577717,
                },
                {
                    mark: '3',
                    type_input: 'экзамен',
                    code_input: 577709,
                },
                {
                    mark: 'зачтено',
                    type_input: 'зачет',
                    code_input: 577719,
                },
                {
                    mark: 'зачтено',
                    type_input: 'зачет',
                    code_input: 577710,
                },
                {
                    mark: '4',
                    type_input: 'практика',
                    code_input: 577714,
                },
            ],
        }
        const res = await request(app).post('/predictBySemester').send({ semesterInfo: badBody })
        assert.deepEqual(res.status, 400)
        assert.deepEqual(res.body.detail, 'Sex must be 1 (male) or 0 (female)')
    })
    it('should be error: Age must be between 17 and 49', async () => {
        const badBody = {
            sex: 1,
            age: 0,
            type_output: 'зачет',
            code_output: 607162,
            userMarks: [
                {
                    mark: 'зачтено',
                    type_input: 'зачет',
                    code_input: 577712,
                },
                {
                    mark: '3',
                    type_input: 'экзамен',
                    code_input: 577713,
                },
                {
                    mark: 'зачтено',
                    type_input: 'зачет',
                    code_input: 577716,
                },
                {
                    mark: 'не явился',
                    type_input: 'зачет',
                    code_input: 577718,
                },
                {
                    mark: '3',
                    type_input: 'экзамен',
                    code_input: 577715,
                },
                {
                    mark: '3',
                    type_input: 'экзамен',
                    code_input: 577711,
                },
                {
                    mark: 'зачтено',
                    type_input: 'зачет',
                    code_input: 577717,
                },
                {
                    mark: '3',
                    type_input: 'экзамен',
                    code_input: 577709,
                },
                {
                    mark: 'зачтено',
                    type_input: 'зачет',
                    code_input: 577719,
                },
                {
                    mark: 'зачтено',
                    type_input: 'зачет',
                    code_input: 577710,
                },
                {
                    mark: '4',
                    type_input: 'практика',
                    code_input: 577714,
                },
            ],
        }
        const res = await request(app).post('/predictBySemester').send({ semesterInfo: badBody })
        assert.deepEqual(res.status, 400)
        assert.deepEqual(res.body.detail, 'Age must be between 17 and 49')
    })
    it('should be error: Mark must be one of', async () => {
        const badBody = {
            sex: 1,
            age: 18,
            type_output: 'зачет',
            code_output: 607162,
            userMarks: [
                {
                    mark: '8',
                    type_input: 'зачет',
                    code_input: 577712,
                },
                {
                    mark: '3',
                    type_input: 'экзамен',
                    code_input: 577713,
                },
                {
                    mark: 'зачтено',
                    type_input: 'зачет',
                    code_input: 577716,
                },
                {
                    mark: 'не явился',
                    type_input: 'зачет',
                    code_input: 577718,
                },
                {
                    mark: '3',
                    type_input: 'экзамен',
                    code_input: 577715,
                },
                {
                    mark: '3',
                    type_input: 'экзамен',
                    code_input: 577711,
                },
                {
                    mark: 'зачтено',
                    type_input: 'зачет',
                    code_input: 577717,
                },
                {
                    mark: '3',
                    type_input: 'экзамен',
                    code_input: 577709,
                },
                {
                    mark: 'зачтено',
                    type_input: 'зачет',
                    code_input: 577719,
                },
                {
                    mark: 'зачтено',
                    type_input: 'зачет',
                    code_input: 577710,
                },
                {
                    mark: '4',
                    type_input: 'практика',
                    code_input: 577714,
                },
            ],
        }
        const res = await request(app).post('/predictBySemester').send({ semesterInfo: badBody })
        assert.deepEqual(res.status, 400)
        assert.deepEqual(res.body.detail, 'Mark must be one of: [нет оценки, не явился, зачтено, не зачтено, не изучал, 2, 3, 4, 5]')
    })
    it('should be error: type_input must be one of', async () => {
        const badBody = {
            sex: 1,
            age: 18,
            type_output: 'зачет',
            code_output: 607162,
            userMarks: [
                {
                    mark: 'зачтено',
                    type_input: 'зачет',
                    code_input: 577712,
                },
                {
                    mark: '3',
                    type_input: 'экзамен',
                    code_input: 577713,
                },
                {
                    mark: 'зачтено',
                    type_input: 'зачет',
                    code_input: 577716,
                },
                {
                    mark: 'не явился',
                    type_input: 'зачет',
                    code_input: 577718,
                },
                {
                    mark: '3',
                    type_input: 'экзамен',
                    code_input: 577715,
                },
                {
                    mark: '3',
                    type_input: 'экзамен',
                    code_input: 577711,
                },
                {
                    mark: 'зачтено',
                    type_input: 'зачет',
                    code_input: 577717,
                },
                {
                    mark: '3',
                    type_input: 'экзамен',
                    code_input: 577709,
                },
                {
                    mark: 'зачтено',
                    type_input: 'зачет',
                    code_input: 577719,
                },
                {
                    mark: 'зачтено',
                    type_input: 'gsegsegse',
                    code_input: 577710,
                },
                {
                    mark: '4',
                    type_input: 'практика',
                    code_input: 577714,
                },
            ],
        }
        const res = await request(app).post('/predictBySemester').send({ semesterInfo: badBody })
        assert.deepEqual(res.status, 400)
        assert.deepEqual(res.body.detail, 'type_input must be one of: [зачет, экзамен, кр, практика, кп]')
    })
    it('should be error: type_output must be one of', async () => {
        const badBody = {
            sex: 1,
            age: 18,
            type_output: 'зdawdawdачет',
            code_output: 607162,
            userMarks: [
                {
                    mark: 'зачтено',
                    type_input: 'зачет',
                    code_input: 577712,
                },
                {
                    mark: '3',
                    type_input: 'экзамен',
                    code_input: 577713,
                },
                {
                    mark: 'зачтено',
                    type_input: 'зачет',
                    code_input: 577716,
                },
                {
                    mark: 'не явился',
                    type_input: 'зачет',
                    code_input: 577718,
                },
                {
                    mark: '3',
                    type_input: 'экзамен',
                    code_input: 577715,
                },
                {
                    mark: '3',
                    type_input: 'экзамен',
                    code_input: 577711,
                },
                {
                    mark: 'зачтено',
                    type_input: 'зачет',
                    code_input: 577717,
                },
                {
                    mark: '3',
                    type_input: 'экзамен',
                    code_input: 577709,
                },
                {
                    mark: 'зачтено',
                    type_input: 'зачет',
                    code_input: 577719,
                },
                {
                    mark: 'зачтено',
                    type_input: 'зачет',
                    code_input: 577710,
                },
                {
                    mark: '4',
                    type_input: 'практика',
                    code_input: 577714,
                },
            ],
        }
        const res = await request(app).post('/predictBySemester').send({ semesterInfo: badBody })
        assert.deepEqual(res.status, 400)
        assert.deepEqual(res.body.detail, 'type_output must be one of: [зачет, экзамен, кр, практика, кп]')
    })
    it('should be error: code_input does not exist', async () => {
        const badBody = {
            sex: 1,
            age: 18,
            type_output: 'зачет',
            code_output: 607162,
            userMarks: [
                {
                    mark: 'зачтено',
                    type_input: 'зачет',
                },
                {
                    mark: '3',
                    type_input: 'экзамен',
                    code_input: 577713,
                },
                {
                    mark: 'зачтено',
                    type_input: 'зачет',
                    code_input: 577716,
                },
                {
                    mark: 'не явился',
                    type_input: 'зачет',
                    code_input: 577718,
                },
                {
                    mark: '3',
                    type_input: 'экзамен',
                    code_input: 577715,
                },
                {
                    mark: '3',
                    type_input: 'экзамен',
                    code_input: 577711,
                },
                {
                    mark: 'зачтено',
                    type_input: 'зачет',
                    code_input: 577717,
                },
                {
                    mark: '3',
                    type_input: 'экзамен',
                    code_input: 577709,
                },
                {
                    mark: 'зачтено',
                    type_input: 'зачет',
                    code_input: 577719,
                },
                {
                    mark: 'зачтено',
                    type_input: 'зачет',
                    code_input: 577710,
                },
                {
                    mark: '4',
                    type_input: 'практика',
                    code_input: 577714,
                },
            ],
        }
        const res = await request(app).post('/predictBySemester').send({ semesterInfo: badBody })
        assert.deepEqual(res.status, 400)
        assert.deepEqual(res.body.detail, 'code_input does not exist')
    })
    it('should be error: code_output does not exist', async () => {
        const badBody = {
            sex: 1,
            age: 18,
            type_output: 'зачет',
            userMarks: [
                {
                    mark: 'зачтено',
                    type_input: 'зачет',
                    code_input: 577712,
                },
                {
                    mark: '3',
                    type_input: 'экзамен',
                    code_input: 577713,
                },
                {
                    mark: 'зачтено',
                    type_input: 'зачет',
                    code_input: 577716,
                },
                {
                    mark: 'не явился',
                    type_input: 'зачет',
                    code_input: 577718,
                },
                {
                    mark: '3',
                    type_input: 'экзамен',
                    code_input: 577715,
                },
                {
                    mark: '3',
                    type_input: 'экзамен',
                    code_input: 577711,
                },
                {
                    mark: 'зачтено',
                    type_input: 'зачет',
                    code_input: 577717,
                },
                {
                    mark: '3',
                    type_input: 'экзамен',
                    code_input: 577709,
                },
                {
                    mark: 'зачтено',
                    type_input: 'зачет',
                    code_input: 577719,
                },
                {
                    mark: 'зачтено',
                    type_input: 'зачет',
                    code_input: 577710,
                },
                {
                    mark: '4',
                    type_input: 'практика',
                    code_input: 577714,
                },
            ],
        }
        const res = await request(app).post('/predictBySemester').send({ semesterInfo: badBody })
        assert.deepEqual(res.status, 400)
        assert.deepEqual(res.body.detail, 'code_output does not exist')
    })
    it('should be error: Query TYPE must be NN or tree (default NN)', async () => {
        const body = {
            sex: 1,
            age: 18,
            type_output: 'зачет',
            code_output: 607162,
            userMarks: [
                {
                    mark: 'зачтено',
                    type_input: 'зачет',
                    code_input: 577712,
                },
                {
                    mark: '3',
                    type_input: 'экзамен',
                    code_input: 577713,
                },
                {
                    mark: 'зачтено',
                    type_input: 'зачет',
                    code_input: 577716,
                },
                {
                    mark: 'не явился',
                    type_input: 'зачет',
                    code_input: 577718,
                },
                {
                    mark: '3',
                    type_input: 'экзамен',
                    code_input: 577715,
                },
                {
                    mark: '3',
                    type_input: 'экзамен',
                    code_input: 577711,
                },
                {
                    mark: 'зачтено',
                    type_input: 'зачет',
                    code_input: 577717,
                },
                {
                    mark: '3',
                    type_input: 'экзамен',
                    code_input: 577709,
                },
                {
                    mark: 'зачтено',
                    type_input: 'зачет',
                    code_input: 577719,
                },
                {
                    mark: 'зачтено',
                    type_input: 'зачет',
                    code_input: 577710,
                },
                {
                    mark: '4',
                    type_input: 'практика',
                    code_input: 577714,
                },
            ],
        }
        const res = await request(app).post('/predictBySemester?type=test').send({ semesterInfo: body })
        assert.deepEqual(res.status, 400)
        assert.deepEqual(res.body.detail, 'Query TYPE must be NN or tree (default NN)')
    })
})
