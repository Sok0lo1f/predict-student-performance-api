import { join, dirname } from 'path'
import { fileURLToPath } from 'url'
import swaggerAutogen from 'swagger-autogen'

const _dirname = dirname(fileURLToPath(import.meta.url))

const doc = {
    info: {
        title: 'Student Performance Prediction API',
        description: 'API for predict student performance',
    },
    definitions: {
        InputSingle: {
            sex: 'number',
            age: 'number',
            mark: 'string',
            type_input: 'string',
            type_output: 'string',
            code_input: 'number',
            code_output: 'number',
        },
        OutputSingle: {
            mark: 'string',
        },
        UserMark: {
            mark: 'string',
            type_input: 'string',
            code_input: 'number',
        },
        InputMultiple: {
            sex: 1,
            age: 18,
            type_output: 'зачет',
            code_output: 'number',
            userMarks: [{ $ref: '#/definitions/UserMark' }],
        },
        MultipleMark: {
            2: 'number',
            3: 'number',
            4: 'number',
            5: 'number',
            'нет оценки': 'number',
            'не явился': 'number',
            зачтено: 'number',
            'не зачтено': 'number',
            'не изучал': 'number',
        },
        OutputMultiple: {
            mark: {
                $ref: '#/definitions/MultipleMark',
            },
        },
    },
    host: 'localhost:3000',
    schemes: ['http'],
}

// путь и название генерируемого файла
const outputFile = join(_dirname, 'output.json')
// массив путей к роутерам
const endpointsFiles = [join(_dirname, '../index_fake.js')]

swaggerAutogen(/*options*/)(outputFile, endpointsFiles, doc).then(({ success }) => {
    console.log(`Generated: ${success}`)
})
